import React from "react";
import { CCard, CCardHeader, CCol, CDataTable, CCardBody } from "@coreui/react";

const fields = ["id", "regra", "cliente", "criado", "atualizado"];

export default function Groups  () {
  return (
      <CCard>
        <CCardHeader>Grupos</CCardHeader>
        <CCardBody>
          <CCol xs="12" lg="12">
            <CCard>
              <CCardBody>
                <CDataTable
                  items={[]}
                  fields={fields}
                  itemsPerPage={10}
                  pagination
                />
              </CCardBody>
            </CCard>
          </CCol>
        </CCardBody>
      </CCard>
  );
};
