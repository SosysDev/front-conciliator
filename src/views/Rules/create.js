import React, {useState, useEffect, useRef} from "react";
import { CCard, CCardHeader, CForm, CFormGroup, CLabel, CInput, CSelect, CButton, CCol, CCardBody } from "@coreui/react";
import api from '../../services/api';
import history from "../../services/history";

export default function RulesCreate  () {
  const [rule, setRule] = useState({
    name: '',
    method_id: 1,
    array_fields: ''
  });
  const [methods, setMethods] = useState([]);

  useEffect(() => {
    async function loadMethods(){
      const response = await api.get(`methods`);
      setMethods(response.data);
    }
    loadMethods();
  }, []);

  function handleChange(e){
    setRule({...rule, method_id: e.targer.value});
  }

  async function handleCreate(){
    const response = await api.post('rules', rule);

    if(response.status === 200){
      history.push('/rules');
    }
  }

  return (
    <>
      <CCard>
        <CCardHeader>Criar Regra</CCardHeader>
        <CCardBody>
          <CCol xs="12" lg="6">
            <CForm>
              <CFormGroup>
                <CLabel>Nome</CLabel>
                <CInput name="name" type="text" onChange={e => setRule({...rule, name: e.target.value})} value={rule.name}/>
              </CFormGroup>
              <CFormGroup>
                <CLabel>Método</CLabel>
                <CSelect name="method_id" onChange={e => handleChange(e)}>
                  {methods.map(method => {
                    return <option key={method.id} value={method.id}>{method.name}</option>
                  })}
                </CSelect>
              </CFormGroup>
              <CFormGroup>
                <CLabel>Campos</CLabel>
                <CInput name="array_fields" type="text" placeholder="field1,field2,..." onChange={e => setRule({...rule, array_fields: e.target.value.split(',')})} value={rule.array_fields}/>
              </CFormGroup>
              <CButton color="success" type="button" onClick={() => handleCreate()}>Salvar</CButton>
            </CForm>
          </CCol>
        </CCardBody>
      </CCard>
      </>
  );
};
