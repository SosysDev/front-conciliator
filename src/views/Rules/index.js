import React, {useEffect, useState} from "react";
import { Link } from 'react-router-dom';
import { CCard, CCardHeader, CCol, CButton, CDataTable, CCardBody } from "@coreui/react";
import api from '../../services/api';

const fields = ["id", "name", "method_id", "created_at", "updated_at"];

export default function Rules  () {
  const [rules, setRules] = useState([]);

  useEffect(() => {
    async function loadRules(){
      const response = await api.get(`rules`);

      setRules(response.data);
    }
    loadRules();
  }, []);

  return (
    <>
      <Link to="/rules/create">Adicionar</Link>
      <CCard>
        <CCardHeader>Regras</CCardHeader>
        <CCardBody>
          <CCol xs="12" lg="12">
            <CCard>
              <CCardBody>
                <CDataTable
                  items={rules}
                  fields={fields}
                  itemsPerPage={10}
                  pagination
                />
              </CCardBody>
            </CCard>
          </CCol>
        </CCardBody>
      </CCard>
      </>
  );
};
