import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";
import "./config/ReactotronConfig";
import "./scss/style.scss";

import history from "./services/history";
import { persistor } from "./store";

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse" />
  </div>
);

const Layout = React.lazy(() => import("./containers/Layout"));
const Login = React.lazy(() => import("./views/Login"));
const Page404 = React.lazy(() => import("./views/page404/Page404"));
const Page500 = React.lazy(() => import("./views/page500/Page500"));

export default function App() {
  return (
    <PersistGate persistor={persistor}>
    <Router history={history}>
      <React.Suspense fallback={loading}>
        <Switch>
          <Route
            exact
            path="/login"
            name="Login Page"
            render={props => <Login {...props} />}
          />
          <Route
            exact
            path="/404"
            name="Page 404"
            render={props => <Page404 {...props} />}
          />
          <Route
            exact
            path="/500"
            name="Page 500"
            render={props => <Page500 {...props} />}
          />
          <Route path="/" name="Home" render={props => <Layout {...props} />} />
        </Switch>
      </React.Suspense>
    </Router>
    </PersistGate>
  );
}
