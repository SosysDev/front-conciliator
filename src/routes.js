import Dashboard from './views/Dashboard';
import Clients from './views/Clients';
import Groups from './views/Groups';
import Rules from './views/Rules/index';
import RulesCreate from './views/Rules/create';

const routes = [
  {
    exact: true,
    path: "/",
    name: "Home",
    component: Dashboard,
    isPrivate: true
  },
  { path: "/clients", exact: true, name: "Clientes", component: Clients , isPrivate: true},
  { path: "/groups", exact: true, name: "Grupos", component: Groups , isPrivate: true},
  { path: "/rules", exact: true, name: "Regras", component: Rules , isPrivate: true},
  { path: "/rules/create", name: "Criar Regra", component: RulesCreate , isPrivate: true},
];

export default routes;
