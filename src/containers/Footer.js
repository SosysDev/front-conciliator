import React from "react";
import { CFooter } from "@coreui/react";

export default function Footer() {
  return (
    <CFooter fixed={false}>
      <div>
        <a href="http://sosys.com.br/" target="_blank" rel="noopener noreferrer">
          Sosys Business Insight
        </a>
      </div>
    </CFooter>
  );
}
