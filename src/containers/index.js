import Content from './Content'
import Footer from './Footer'
import Header from './Header'
import Settings from './Settings'
import Layout from './Layout'
import Menu from './Menu'

export {
  Content,
  Footer,
  Header,
  Settings,
  Layout,
  Menu
}
