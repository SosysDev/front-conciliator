import React, { Suspense } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { CContainer, CFade } from "@coreui/react";
import { store } from "../store";
import routes from "../routes";

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse" />
  </div>
);

export default function Content() {
  function authenticate(isPrivate) {
    const { signed } = store.getState().auth;

    if (!signed && isPrivate) {
      return false;
    }

    return true;
  }

  return (
    <main className="c-main">
      <CContainer fluid>
        <Suspense fallback={loading}>
          <Switch>
            {routes.map((route, idx) => {
              return (
                route.component && (
                  <Route
                    key={idx}
                    path={route.path}
                    exact={route.exact}
                    name={route.name}
                    render={props =>
                      authenticate(route.isPrivate) ? (
                        <CFade>
                          <route.component {...props} />
                        </CFade>
                      ) : (
                        <Redirect to={{ pathname: "/login" }} />
                      )
                    }
                  />
                )
              );
            })}
            <Redirect to="/" />
          </Switch>
        </Suspense>
      </CContainer>
    </main>
  );
}
