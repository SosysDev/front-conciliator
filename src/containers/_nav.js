export default [
  {
    _tag: "CSidebarNavItem",
    name: "Clientes",
    to: "/clients",
    icon: "cil-cursor"
  },
  {
    _tag: "CSidebarNavItem",
    name: "Grupos",
    to: "/groups",
    icon: "cil-people"
  },
  {
    _tag: "CSidebarNavItem",
    name: "Regras",
    to: "/rules",
    icon: "cil-file"
  }
];
