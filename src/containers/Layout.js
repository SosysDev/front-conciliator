import React from "react";
import { Content, Menu, Footer, Header } from "./index";

export default function Layout() {
  return (
    <div className="c-app c-default-layout">
      <Menu />
      <div className="c-wrapper">
        <Header />
        <div className="c-body">
          <Content />
        </div>
        <Footer />
      </div>
    </div>
  );
}
